import 'package:dartz/dartz.dart';

import '../entities/search_entity.dart';

abstract class SearchMoviesUseCase {
  Future<Either<Exception, SearchEntity>> call(String query);
}

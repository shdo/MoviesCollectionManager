import 'package:dartz/dartz.dart';

import '../entities/search_entity.dart';
import '../repositories/search_movies_repository.dart';
import 'search_movies_usecase.dart';

class SearchMoviesUseCaseImp implements SearchMoviesUseCase {
  final SearchMoviesRepository _searchMoviesRepository;
  SearchMoviesUseCaseImp(this._searchMoviesRepository);

  @override
  Future<Either<Exception, SearchEntity>> call(query) async {
    return await _searchMoviesRepository(query);
  }
}

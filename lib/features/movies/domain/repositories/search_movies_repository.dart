import 'package:dartz/dartz.dart';

import '/features/movies/domain/entities/search_entity.dart';

abstract class SearchMoviesRepository {
  Future<Either<Exception, SearchEntity>> call(query);
}

import 'package:movieapp/features/movies/domain/entities/search_results_entity.dart';

class SearchEntity {
  SearchEntity({
    required this.page,
    required this.results,
    required this.totalResults,
    required this.totalPages,
  });
  late final int page;
  late final List<SearchResultsEntity> results;
  late final int totalResults;
  late final int totalPages;
}

import 'package:dartz/dartz.dart';
import '../datasources/search_movies_datasource.dart';
import '../../domain/entities/search_entity.dart';
import '../../domain/repositories/search_movies_repository.dart';

class SearchMoviesRepositoryImp implements SearchMoviesRepository {
  final SearchMoviesDataSource _searchMoviesDataSource;
  SearchMoviesRepositoryImp(this._searchMoviesDataSource);

  @override
  Future<Either<Exception, SearchEntity>> call(query) async {
    return await _searchMoviesDataSource(query);
  }
}

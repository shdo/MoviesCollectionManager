import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/features/movies/data/datasources/local/search_movies_datasource_decorator.dart';
import '/features/movies/data/datasources/search_movies_datasource.dart';
import '/features/movies/domain/entities/search_entity.dart';
import '/features/movies/data/dtos/search_dto.dart';

const moviesSearchCache = "MOVIES_SEARCH_CACHE";

class SearchMoviesLocalDataSourceDecoratorImp
    extends SearchMoviesDataSourceDecorator {
  SearchMoviesLocalDataSourceDecoratorImp(
      SearchMoviesDataSource searchMoviesDataSource)
      : super(searchMoviesDataSource);

  @override
  Future<Either<Exception, SearchEntity>> call(query) async {
    return (await super(query)).fold(
      (error) async {
        if (await _isThereCache()) {
          return Right(await _getFromCache());
        }
        print("ERRO decorator: " + error.toString());
        return Left(Exception("Search is not in cache"));
      },
      (result) {
        _saveInCache(result);
        return Right(result);
      },
    );
  }

  _saveInCache(SearchEntity movies) async {
    var prefs = await SharedPreferences.getInstance();
    String jsonMovies = jsonEncode(movies.toJson());
    prefs.setString(moviesSearchCache, jsonMovies);
    if (kDebugMode) {
      print('Search saved on cache' + jsonMovies);
    }
  }

  Future<bool> _isThereCache() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(moviesSearchCache)) {
      return true;
    }
    return false;
  }

  Future<SearchEntity> _getFromCache() async {
    var prefs = await SharedPreferences.getInstance();
    var moviesJsonString = prefs.getString(moviesSearchCache)!;
    var json = jsonDecode(moviesJsonString);
    var movies = SearchDto.fromJson(json);
    if (kDebugMode) {
      print('Search from cache the movies ' + movies.toString());
    }
    return movies;
  }
}

import 'package:dartz/dartz.dart';

import '/features/movies/data/datasources/search_movies_datasource.dart';
import '/features/movies/domain/entities/search_entity.dart';

class SearchMoviesDataSourceDecorator implements SearchMoviesDataSource {
  final SearchMoviesDataSource _searchMoviesDataSource;

  SearchMoviesDataSourceDecorator(this._searchMoviesDataSource);

  @override
  Future<Either<Exception, SearchEntity>> call(query) =>
      _searchMoviesDataSource(query);
}

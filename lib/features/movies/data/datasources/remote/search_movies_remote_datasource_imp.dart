import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

import '/core/domain/services/http_service.dart';
import '/core/utils/api_utils.dart';
import '/core/utils/os_info_utils.dart';
import '/features/movies/data/datasources/search_movies_datasource.dart';
import '/features/movies/data/dtos/search_dto.dart';
import '/features/movies/domain/entities/search_entity.dart';

class SearchMoviesRemoteDatasourceImp implements SearchMoviesDataSource {
  final HttpService _httpService;

  SearchMoviesRemoteDatasourceImp(this._httpService);

  @override
  Future<Either<Exception, SearchEntity>> call(query) async {
    try {
      if (kDebugMode) {
        await Future.delayed(const Duration(seconds: 3));
      }
      var result =
          await _httpService.get(API.searchMovie(query, OSInfo().getLocale()));
      print(" START API ");
      print(result.data);
      print(" END API ");
      return Right(SearchDto.fromJson(result.data));
    } catch (e) {
      print("ERROR RemoteDataSourceImp: " + e.toString());
      return Left(Exception('Search no answer'));
    }
  }
}

import 'package:dartz/dartz.dart';
import '../../domain/entities/search_entity.dart';


abstract class SearchMoviesDataSource {
  Future<Either<Exception, SearchEntity>> call(query);
}

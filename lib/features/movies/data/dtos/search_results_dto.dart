import '../../domain/entities/search_results_entity.dart';


// TODO resolver ERROR RemoteDataSourceImp: type 'Null' is not a subtype of type 'int'

extension SearchResultsDto on SearchResultsEntity {
  static SearchResultsEntity fromJson(Map json) {
    return SearchResultsEntity(
      posterPath: json['posterPath'],
      adult: json['adult'],
      overview: json['overview'],
      releaseDate: json['release_date'],
      genreIds: List.castFrom<dynamic, int>(json['genre_ids']),
      id: json['id'],
      originalTitle: json['original_title'],
      originalLanguage: json['original_language'],
      title: json['title'],
      backdropPath: json['backdropPath'],
      popularity: json['popularity'],
      voteCount: json['vote_count'],
      video: json['video'],
      voteAverage: json['vote_average'].toDouble(),
    );
  }

  Map toJson() {
    return {
      'poster_path': posterPath,
      'adult': adult,
      'overview': overview,
      'release_date': releaseDate,
      'genre_ids': genreIds,
      'id': id,
      'original_title': originalTitle,
      'original_language': originalLanguage,
      'title': title,
      'backdrop_path': backdropPath,
      'popularity': popularity,
      'vote_count': voteCount,
      'video': video,
      'vote_average': voteAverage,
    };
  }
}

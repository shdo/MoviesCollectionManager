import 'dart:convert';

import 'package:movieapp/features/movies/data/dtos/search_results_dto.dart';
import 'package:movieapp/features/movies/domain/entities/search_entity.dart';

extension SearchDto on SearchEntity {
  Map toMap() {
    return {
      'page': page,
      'results': results.map((e) => e.toJson()).toList(),
      'totalResults': totalResults,
      'totalPages': totalPages,
    };
  }

  String toJson() => json.encode(toMap());

  static SearchEntity fromJson(Map json) {
    return SearchEntity(
      page: json['page'],
      //results: json['results'] as List<SearchResultsEntity>,
      results: List.from(json['results'])
          .map((e) => SearchResultsDto.fromJson(e))
          .toList(),
      totalResults: json['totalResults'],
      totalPages: json['totalPages'],
    );
  }
}

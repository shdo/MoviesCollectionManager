/*
navigation_page.dart
@author Sérgio Henrique D. de Oliveira
@version 1.0.193
*/

import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:movieapp/core/utils/device_screen_info_utils.dart';

import '/features/movies/presentation/ui/components/window_buttons.dart';
import '/features/movies/presentation/ui/views/pages/account_page.dart';
import '/features/movies/presentation/ui/views/pages/collections_page.dart';
import '/features/movies/presentation/ui/views/pages/movies_page.dart';

/// Perfoms the navigation between pages
///
/// The screen width is always analysed to determine if the components fit and
///
/// On large screens Navigation Rails is used
///
///     | LARGE LANDSCAPE LAYOUT --------|
///     |           |                    |
///     | NAV. RAIL |        PAGE        |
///     |           |                    |
///     |--------------------------------|
///
///
/// On small screens Bottom Navigation is used
///
///     | SMALL LAYOUT -----|
///     |                   |
///     |                   |
///     |                   |
///     |                   |
///     |       PAGE        |
///     |                   |
///     |                   |
///     |                   |
///     |                   |
///     |-------------------|
///     |    BOTTOM NAV.    |
///     |-------------------|
///
////////////////////////////////////////////////////////////////////////////
class NavigationPage extends StatefulWidget {
  const NavigationPage({Key? key}) : super(key: key);

  static const routeName = '/';

  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  int _tabselected = 0;

  final pages = [
    const MoviesPage(),
    const CollectionsPage(),
    const AccountPage(),
  ];

  Widget appIcon = Image.asset('assets/appIcon.png', fit: BoxFit.cover);

  /// Navigation Bar/Rail
  late List<NavigationDestination> bottomNavigationBarDestinations;
  late List<NavigationRailDestination> navigationRailDestinations;

  /// NavigationBar/Rail Icons
  Widget iconMovies = const Icon(Icons.movie_filter_outlined);
  Widget iconMoviesSel = const Icon(Icons.movie_creation_rounded);

  Widget iconCollections = const Icon(Icons.video_library_outlined);
  Widget iconCollectionsSel = const Icon(Icons.video_library_rounded);

  Widget iconAccount = const Icon(Icons.account_circle_outlined);
  Widget iconAccountSel = const Icon(Icons.account_circle_rounded);

  /// Color of NavigationRail, BottomNavigationBar and WindowBorder
  late Color barsColor;

  @override
  Widget build(BuildContext context) {
    barsColor = Theme.of(context).colorScheme.onInverseSurface;

    /// Window title
    String windowTitle = AppLocalizations.of(context)!.appTitle;

    /// NavigationBar/Rail button labels
    String strMovies = AppLocalizations.of(context)!.movies;
    String strCollections = AppLocalizations.of(context)!.collections;
    String strAccount = AppLocalizations.of(context)!.account;

    bottomNavigationBarDestinations = [
      NavigationDestination(
        icon: iconMovies,
        label: strMovies,
        selectedIcon: iconMoviesSel,
      ),
      NavigationDestination(
        icon: iconCollections,
        label: strCollections,
        selectedIcon: iconCollectionsSel,
      ),
      NavigationDestination(
        icon: iconAccount,
        label: strAccount,
        selectedIcon: iconAccountSel,
      ),
    ];

    navigationRailDestinations = [
      NavigationRailDestination(
        icon: iconMovies,
        label: Text(strMovies),
        selectedIcon: iconMoviesSel,
      ),
      NavigationRailDestination(
        icon: iconCollections,
        label: Text(strCollections),
        selectedIcon: iconCollectionsSel,
      ),
      NavigationRailDestination(
        icon: iconAccount,
        label: Text(strAccount),
        selectedIcon: iconAccountSel,
      ),
    ];

    return SafeArea(
      child: Scaffold(
        drawer: Container(),
        body: getWindowFrame(
          body: _mainBody(
              navigationRailWidth:
                  MediaQuery.of(context).size.width > DeviceScreenInfoUtils.hd
                      ? 86
                      : 72,
              widthIsLarge: DeviceScreenInfoUtils.isLarge(context)),
          windowTitle: windowTitle,
        ),
        bottomNavigationBar: !DeviceScreenInfoUtils.isLarge(context)
            ? NavigationBarTheme(
                data: NavigationBarThemeData(
                  backgroundColor: barsColor,
                ),
                child: NavigationBar(
                  selectedIndex: _tabselected,
                  labelBehavior: NavigationDestinationLabelBehavior.alwaysShow,
                  animationDuration: const Duration(seconds: 1),
                  onDestinationSelected: (index) {
                    setState(() {
                      _tabselected = index;
                    });
                  },
                  destinations: bottomNavigationBarDestinations,
                ),
              )
            : null,
      ),
    );
  }

  /// If the app is running in a desktop puts a themed window title bar
  Widget getWindowFrame({required Widget body, required String windowTitle}) {
    if (DeviceScreenInfoUtils.isDesktop) {
      return Column(
        children: [
          /*!DeviceScreenInfoUtils.isHD(context) ?*/
          WindowTitleBarBox(
            child: Container(
              //color: frameColor,

              /// Top line with icon, name and command buttons
              child: Row(
                children: [
                  Expanded(
                      child: MoveWindow(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          width: 4,
                        ),

                        /// Topbar AppIcon
                        SizedBox(
                          width: 16,
                          height: 16,
                          child: FittedBox(
                            child: Image.asset('assets/icon.png'),
                            fit: BoxFit.fill,
                          ),
                        ),

                        /*IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.movie_creation_rounded,
                            size: 16,
                          ),
                        ),*/
                        const SizedBox(
                          width: 4,
                        ),
                        Text(windowTitle),
                      ],
                    ),
                  )),
                  const WindowButtons()
                ],
              ),
            ),
          ) /*: Container()*/,
          Expanded(
            child: body,
          ),
        ],
      );
    } else {
      return body;
    }
  }

  /// Main body contains the page viewer and the NavigationRail if the screen is large
  Widget _mainBody(
      {required double navigationRailWidth, required bool widthIsLarge}) {
    return Row(
      children: [
        widthIsLarge
            ? NavigationRail(
                /// Center the buttons vertically
                groupAlignment: -0.2,

                /// Drawer menu icon (hamburguer icon / bars menu)
                leading: IconButton(
                  icon: const Icon(Icons.menu_rounded),
                  onPressed: () {},
                ),

                //backgroundColor: frameColor,

                // TODO inverter as dependencias e colocar como parametro o DeviceScreenInfoUtils para determinar o width
                minWidth: navigationRailWidth,
                selectedIndex: _tabselected,
                labelType: NavigationRailLabelType.all,
                onDestinationSelected: (index) {
                  setState(() {
                    _tabselected = index;
                  });
                },
                destinations: navigationRailDestinations,
              )
            : Container(),
        Expanded(
          child: AnimatedSwitcher(
            /*duration: const Duration(milliseconds: 500),*/
            duration: kThemeAnimationDuration,
            child: pages[_tabselected],
          ),
        ),
      ],
    );
  }
}

import 'dart:io';

class OSInfo {
  final String defaultLocale = Platform.localeName;

  String getLocale() {
    return defaultLocale.replaceAll("_", "-");
  }
}

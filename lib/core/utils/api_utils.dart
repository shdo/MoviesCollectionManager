/*
THE MARVEL UNIVERSE LIST: list/1
OSCAR LIST: list/2
 */
class API {
  static String requestImg(String img) =>
      'https://image.tmdb.org/t/p/w500/$img';

  //static const requestMovieList = 'list/1?page=1&language=pt-BR';
  static String requestMovieList(String locale) =>
      'list/1?page=1&language=$locale';

// TODO incluir a paginação
  static String searchMovie(String query, String locale) {
//enc =  Uri.encodeComponent(query);

    return 'search/movie?query=$query&language=$locale&page=1&include_adult=false';
  }
}
